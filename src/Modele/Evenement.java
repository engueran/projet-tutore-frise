package Modele;

public class Evenement implements Comparable <Evenement>{
	private String chTitre;
	private int chJour;
	private int chMois;
	private int chAnnee;
	private String chImage;
	private String chDesc;
	private int chPoid;
	
	public Evenement(String parTitre, int parJour, int parMois, int parAnnee, String parImage, String parDesc, int parPoid){
		chTitre = parTitre;
		chJour = parJour;
		chMois = parMois;
		chAnnee = parAnnee;
		chImage = parImage;
		chDesc = parDesc;
		chPoid = parPoid;
	}
	
	/*public Evenement(String parTitre, int parJour, int parMois, int parAnnee, String parImage, String parDesc) {
		chTitre = parTitre;
		chJour = parJour;
		chMois = parMois;
		chAnnee = parAnnee;
		chImage = parImage;
		chDesc = parDesc;
	}*/
	
	public String getChTitre() {
		return chTitre;
	}
	
	public int getChJour() {
		return chJour;
	}
	
	public int getChMois() {
		return chMois;
	}
	
	public int getChAnnee() {
		return chAnnee;
	}
	
	public String getChDesc() {
		return chDesc;
	}
	
	public String getChImage() {
		return chImage;
	}
	
	public int getChPoid() {
		return chPoid;
	}
	
	public void setChTitre(String parTitre) {
		chTitre = parTitre;
	}
	
	public void setChJour(int parJour) {
		chJour = parJour;
	}
	
	public void setChMois(int parMois) {
		chMois = parMois;
	}
	
	public void setChAnnee(int parAnnee) {
		chAnnee = parAnnee;
	}
	
	public void setChDesc(String parDesc) {
		chDesc = parDesc;
	}
	
	public void setChImage(String parImage) {
		chImage = parImage;
	}
	
	public void setChPoid(int parPoid) {
		chPoid = parPoid;
	}
	
    public int compareTo (Evenement parEvenement){
    	if(this.chAnnee < parEvenement.chAnnee){
    		return -1;
    	}
    	else
    		if(this.chAnnee > parEvenement.chAnnee){
    			return 1;
    		}
    		else{
    			if(this.chMois < parEvenement.chMois){
    				return -1;
    			}
    			else{
    				if(this.chMois > parEvenement.chMois){
    					return 1;
    				}
    				else{
    					if(this.chJour < parEvenement.chJour){
    						return -1;
    					}
    					else{
    						if(this.chJour > parEvenement.chJour){
    							return 1;
    						}
    						else{
    							return 0;
    						}
    					}//else jour
    				}//else jour
    			}//else mois
    		}//else annee
    }	
	public String toString() {
		return "Titre : " + chTitre + " Date : " + chJour + "/" + chMois + "/" + chAnnee + " Description : " + chDesc + " Poids : " + chPoid;
	}
}
