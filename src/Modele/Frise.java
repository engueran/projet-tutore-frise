package Modele;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

public class Frise {
	String chTitre;
	int chAnneeDebut;
	Evenement tabEvenement [];
	int TAILLE_TABLEAU;
	int nbEvenement;
	
	private TreeSet <Evenement> arbreEvenement;
	private TreeMap <Integer, TreeSet<Evenement>> treeMapEvenement;
	
	public Frise(String parTitre, int parAnneeDebut) {
		chTitre = parTitre;
		chAnneeDebut = parAnneeDebut;
		arbreEvenement = new TreeSet <Evenement>();
		treeMapEvenement = new TreeMap <Integer, TreeSet<Evenement>>();
	}
	
	
	public void ajout (Evenement parEvenement) {
		int annee = parEvenement.getChAnnee();
		if (treeMapEvenement.containsKey(annee) == true) {
			TreeSet unSet = treeMapEvenement.get(annee);
			unSet.add(parEvenement);
		}
		else {
			TreeSet setNew = new TreeSet();
			setNew.add(parEvenement);
			treeMapEvenement.put(annee, setNew);
		}
	}
	
	public TreeSet<Evenement> getEvts(int parAnnee) {
		return treeMapEvenement.get(parAnnee);
	}
	
	public String toString() {
		return treeMapEvenement.toString();
	}
	
	public String getTitre(){
		return chTitre;
	}
	
	public int getAnneeDebut(){
    	return  chAnneeDebut ;
    }
	public static void main(String [] args) {
		Frise NomDeLaFrise = new Frise("voiture", 1960);
		NomDeLaFrise.ajout(new Evenement("Noel2018", 25, 12, 2018, "/Document/Image", "c'est la noel de 2018", 3));
		NomDeLaFrise.ajout(new Evenement("Anniv Engueran", 14, 10, 1999, "/Document/Image", "c'est l'anniversaire de Engueran", 4));
		NomDeLaFrise.ajout(new Evenement("Anniv", 14, 9, 1999, "/Documenage", "c'est l'anniv Engueran", 3));
		
		System.out.println(NomDeLaFrise.toString());
	}
}
