package Modele;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.table.DefaultTableModel;

import Modele.Evenement;
import Modele.Frise;
import Vue.PanelFormulaireFrise;

public class ModeleTable extends DefaultTableModel {
   
	int tailleTab = 2021;
	int dateDebut = 1970 ;
    public ModeleTable (Frise parFrise) {
    	String [] tabAnnee = new String [tailleTab - dateDebut];
    	for (int i = 0; i < tailleTab - dateDebut; i++) {
    		int a = dateDebut+i ;
    		if (i % 5 == 0) {
    			tabAnnee[i] = ""+ a;
    		}
    		else {
    			tabAnnee[i] = " ";
    		}
    	}
    	Evenement Anniversaire = new Evenement("Anniversaire", 25, 12, 1980, "/Document/Image", "c'est l'anniversaire de victor", 1);
    	Evenement Concours = new Evenement("Concours", 25, 12, 1978, "/Document/Image", "c'est le concours", 2);
    	Evenement Noel2018 = new Evenement("Noel2018", 25, 12, 2018, "/Document/Image", "c'est la noel de 2018", 3);
		Evenement Presentation = new Evenement("Presentation", 14, 9, 1999, "/Documenage", "c'est la Presentation", 4);
    	this.setColumnCount(tailleTab - dateDebut);
		this.setRowCount(4);
    	this.setColumnIdentifiers(tabAnnee);
    	this.setValueAt(Anniversaire.getChTitre(),Anniversaire.getChPoid()-1,Anniversaire.getChAnnee()-dateDebut);
    	this.setValueAt(Noel2018.getChTitre(),Noel2018.getChPoid()-1,Noel2018.getChAnnee()-dateDebut);
    	this.setValueAt(Concours.getChTitre(),Concours.getChPoid()-1,Concours.getChAnnee()-dateDebut);
    	this.setValueAt(Presentation.getChTitre(),Presentation.getChPoid()-1,Presentation.getChAnnee()-dateDebut);

    	
    	
    	
    }
    
    
    /**
     * 
     */
    public boolean isCellEditable (int indiceLigne, int indiceColonne){
		return false;
	}
}

