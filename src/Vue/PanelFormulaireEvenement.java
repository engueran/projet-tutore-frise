package Vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Modele.Evenement;
import Modele.Frise;
import Modele.LectureEcriture;
import Modele.ModeleTable;

public class PanelFormulaireEvenement extends JPanel implements ActionListener {
	Frise frise = new Frise("voiture",1960);
	ModeleTable Frisealpha = new ModeleTable(frise);
	private String TitreBouton = "Ajouter l'evenement a la frise";
	private JButton chBouton = new JButton();
	JTextField titre = new JTextField(20);

	JTextField jour = new JTextField(2);
	
	JTextField mois = new JTextField(2);
	
	JTextField annee = new JTextField(4);
	
	JTextField image = new JTextField(20);
	JTextArea descritption = new JTextArea(10, 10);
	JTextField poid = new JTextField(2);
	public PanelFormulaireEvenement() {
		chBouton = new JButton(TitreBouton);
		chBouton.addActionListener(this);
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints contraintes = new GridBagConstraints();
		contraintes.fill = GridBagConstraints.BOTH;
		contraintes.insets = new Insets(10, 10, 10, 10);
		
		JLabel etiquettePanel = new JLabel("Cr�ation d'un nouvel �venement");
		contraintes.gridx = 0;
		contraintes.gridy = 0;
		this.add(etiquettePanel, contraintes);
		
		/*Titre de l'�venement*/
		JLabel etiquetteTitre = new JLabel("Titre : ");
		contraintes.gridx = 0;
		contraintes.gridy = 2;
		this.add(etiquetteTitre, contraintes);
		
		contraintes.gridx = 1;
		contraintes.gridy = 2;
		this.add(titre, contraintes);
		
		/*Jour de l'�venement*/
		JLabel etiquetteJour = new JLabel("Jour (JJ): ");
		contraintes.gridx = 0;
		contraintes.gridy = 3;
		this.add(etiquetteJour, contraintes);
		
		contraintes.gridx = 1;
		contraintes.gridy = 3;
		this.add(jour, contraintes);
		
		/*Mois de l'�venement*/
		JLabel etiquetteMois = new JLabel("Mois (MM): ");
		contraintes.gridx = 2;
		contraintes.gridy = 3;
		this.add(etiquetteMois, contraintes);
		
		contraintes.gridx = 3;
		contraintes.gridy = 3;
		this.add(mois, contraintes);
		
		/*Ann�e de l'�venement*/
		JLabel etiquetteAnnee = new JLabel("Ann�e (AAAA): ");
		contraintes.gridx = 4;
		contraintes.gridy = 3;
		this.add(etiquetteAnnee, contraintes);
		
		contraintes.gridx = 5;
		contraintes.gridy = 3;
		this.add(annee, contraintes);
		
		/*Adresse de l'image*/
		JLabel etiquetteImage = new JLabel("Adresse Image : ");
		contraintes.gridx = 0;
		contraintes.gridy = 4;
		this.add(etiquetteImage, contraintes);
		
		contraintes.gridx = 1;
		contraintes.gridy = 4;
		this.add(image, contraintes);
		
		/*Description de l'�venement*/
		JLabel etiquetteDescription = new JLabel("Description : ");
		contraintes.gridx = 0;
		contraintes.gridy = 5;
		this.add(etiquetteDescription, contraintes);
		
		contraintes.gridx = 1;
		contraintes.gridy = 5;
		this.add(descritption, contraintes);
		
		/*Poid de l'�venement*/		
	
		JLabel etiquettePoid = new JLabel("Poids (1 � 4): ");
		contraintes.gridx = 0;
		contraintes.gridy = 6;
		this.add(etiquettePoid, contraintes);
		
		contraintes.gridx = 1;
		contraintes.gridy = 6;
		this.add(poid, contraintes);
		
		contraintes.gridx = 0;
		contraintes.gridy = 7;
		this.add(chBouton, contraintes);
	}
	
	public Evenement getEvenement () {
		int jour1 = Integer.parseInt(jour.getText());
		int annee1 = Integer.parseInt(annee.getText());
		int mois1 = Integer.parseInt(mois.getText());
		int poid1 = Integer.parseInt(poid.getText());

		
		return (new Evenement(titre.getText(), jour1, mois1, annee1, image.getText(), descritption.getText(),poid1));
	}
	public String getTitreEvt () {
		
		return titre.getText();
	}
	public void actionPerformed(ActionEvent ajoutEvenement) {
		if(ajoutEvenement.getSource() == chBouton) {
			frise.ajout(getEvenement());
			System.out.println(frise.toString());
			int jour1 = Integer.parseInt(jour.getText());
			int annee1 = Integer.parseInt(annee.getText());
			int mois1 = Integer.parseInt(mois.getText());
			int poid1 = Integer.parseInt(poid.getText());
			
			Frisealpha.setValueAt(this.getEvenement().getChTitre(),poid1-1,annee1-1970);

		}
	}
}
