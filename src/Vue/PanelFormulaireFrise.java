package Vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Modele.Frise;
import Modele.LectureEcriture;

public class PanelFormulaireFrise extends JPanel implements ActionListener {
	Frise frise;
	private String TitreBouton = "Ajouter la frise";
	private JButton chBouton = new JButton();
	JTextField titre = new JTextField(20);

	JTextField anneeDebut = new JTextField(4);

	public PanelFormulaireFrise(){
		chBouton = new JButton(TitreBouton);
		chBouton.addActionListener(this);
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints contraintes = new GridBagConstraints();
		contraintes.fill = GridBagConstraints.BOTH;
		contraintes.insets = new Insets(10, 10, 10, 10);
		
		JLabel etiquetteFrise = new JLabel("Cr�ation d'une nouvelle frise");
		contraintes.gridx = 0;
		contraintes.gridy = 0;
		this.add(etiquetteFrise, contraintes);
		
		contraintes.gridx = 0;
		contraintes.gridy = 4;
		this.add(chBouton, contraintes);
		
		/*Titre de l'�venement*/
		JLabel etiquetteTitre = new JLabel("Titre : ");
		contraintes.gridx = 0;
		contraintes.gridy = 2;
		this.add(etiquetteTitre, contraintes);
		
		contraintes.gridx = 1;
		contraintes.gridy = 2;
		this.add(titre, contraintes);
		
		/*Ann�e debut evenement*/
		JLabel etiquetteAnnee = new JLabel("Ann�e d�but : ");
		contraintes.gridx = 0;
		contraintes.gridy = 3;
		this.add(etiquetteAnnee, contraintes);
		
		contraintes.gridx = 1;
		contraintes.gridy = 3;
		this.add(anneeDebut, contraintes);
		
		
	}
	
	public void actionPerformed(ActionEvent ajoutFrise) {
		if(ajoutFrise.getSource() == chBouton) {
			/*int anneeDebut1 = Integer.parseInt(anneeDebut.getText());*/
			LectureEcriture.ecriture(new File("repertoire"+File.separator+titre.getText()+".ser"),frise);
			/*return new Frise(titre.getText(), anneeDebut1);*/
		}
	}
}

