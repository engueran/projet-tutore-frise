package Vue;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Modele.Evenement;
import Modele.Frise;
import Modele.ModeleTable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;


public class FenetreMere extends JFrame implements Serializable,ActionListener{
	

	//Cr�ation du menu
	JMenuBar menuBar = new JMenuBar();
	//Choix dans le menu
	
			//Cr�ation
			JMenu Creation = new JMenu("Cr�er");
			JMenuItem ChoixCreationFrise = new JMenuItem("Cr�er une frise");
			JMenuItem ChoixCreationEvenement = new JMenuItem("Cr�er un �venement");
			//Affichage
			JMenu Affichage = new JMenu("Afficher");
			JMenuItem ChoixAffichageEvenement = new JMenuItem("Affichage d'un �venement");
			//Aide
			JMenu ChoixAide = new JMenu("Aide");
			//Annulation de l'action en cours
			JMenu Annuler = new JMenu("Annuler");
			
			PanelFormulaireEvenement contentPane = new PanelFormulaireEvenement() ;
			JPanel CreationFrise = new PanelFormulaireFrise();
			JPanel CreationEvenement = new PanelFormulaireEvenement();
			PanelEvenement AfficheEvt = new PanelEvenement();
			
			
	public FenetreMere() {
		super();
		this.setContentPane(contentPane);
		contentPane.setBackground(new Color(225,225,225));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(1200,800);
		this.setLocation(150,150);
		this.Creation.add(ChoixCreationFrise);
		this.Creation.add(ChoixCreationEvenement);
		this.Affichage.add(ChoixAffichageEvenement);
		this.menuBar.add(Creation);
		this.menuBar.add(Affichage);
		this.menuBar.add(ChoixAide);
		this.EcouteurMenu();
		this.setJMenuBar(menuBar);
		this.setVisible(true);
	}

	
	public void EcouteurMenu(){
		ChoixCreationFrise.addActionListener(this);
		ChoixCreationEvenement.addActionListener(this);
		ChoixAffichageEvenement.addActionListener(this);
		Affichage.addActionListener(this);
		ChoixAide.addActionListener(this);	
	}

	
	public void actionPerformed(ActionEvent choix) {
    	
  		if(choix.getSource()==ChoixCreationFrise){
				ChoixCreationFrise.setBackground(new Color(225,225,225));
		 		this.setContentPane(CreationFrise);
			   	this.setVisible(true);   
		}
  		
  		if(choix.getSource()==ChoixCreationEvenement){
  				CreationEvenement.setBackground(new Color(225,225,225));
			 	this.setContentPane(CreationEvenement);
				this.setVisible(true);
		}
  		
		
	  	if(choix.getSource()==ChoixAffichageEvenement){
	 		this.setContentPane(AfficheEvt);
		   	this.setVisible(true);
	  	}
      }
	
	
	
	
	
	public Insets getInsets() {
		return new Insets(30,5,0,0);
		}
	
	public static void main(String [] args) {
		new FenetreMere();
		Frise NomDeLaFrise = new Frise("voiture",1960);
		
		NomDeLaFrise.ajout(new Evenement("Concours", 25, 12, 1978, "/Document/Image", "c'est le concours", 2));
		NomDeLaFrise.ajout(new Evenement("Anniversaire", 25, 12, 1980, "/Document/Image", "c'est l'anniversaire de victor", 1));
		NomDeLaFrise.ajout(new Evenement("Presentation", 14, 9, 1999, "/Documenage", "c'est la Presentation", 4));
		NomDeLaFrise.ajout(new Evenement("Noel2018", 25, 12, 2018, "/Document/Image", "c'est la noel de 2018", 3));
		System.out.println(NomDeLaFrise.toString());
		}
	}

