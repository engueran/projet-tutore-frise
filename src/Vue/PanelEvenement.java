package Vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Modele.Frise;
import Modele.ModeleTable;

public class PanelEvenement extends JPanel implements ActionListener{
	
	private String BoutonSuivant = ">";
	private String BoutonPrecedent = "<";
	private JLabel titrefrise = new JLabel("Evenement entre 1970 et 2020");
	private JLabel imagefrise = new JLabel("Description de l'evenement");
	private JLabel contenufrise = new JLabel("contenufrise");
	private JPanel PanelText = new JPanel();
	private JPanel Panelimage = new JPanel();
	private JButton chBoutonSuivant = new JButton() ;
	private JButton chBoutonPrecedent = new JButton() ;
	private CardLayout CardLayoutImage = new CardLayout() ;
	private CardLayout CardLayoutText = new CardLayout() ;
	private Frise NomDeLaFrise = new Frise("voiture",1960);
	private ModeleTable AfficheFrise = new ModeleTable(NomDeLaFrise);
	private File repertoire = new File ("images");
	private String [] intitules_images = repertoire.list();
	private String [] titreImage = {"Concours de 3h entre 8h et 11h" ,"L'anniversaire de  14h � 19h"," Pr�sentation orale � 15h", "Acheter cadeaux pour no�l"};
	private JLabel tabLabels [] = new JLabel[intitules_images.length] ;
	private JLabel tabLabelsTitre [] = new JLabel[intitules_images.length] ;
	GridBagConstraints contrainte = new GridBagConstraints();
	
	
	public PanelEvenement() {
		this.setLayout (new GridBagLayout()) ;
		
		//bouton precedent col 0 ligne 2-3
		chBoutonPrecedent = new JButton(BoutonPrecedent);
		chBoutonPrecedent.addActionListener(this);
		contrainte.gridx = 0 ;
		contrainte.gridy = 1 ;
		this.add(chBoutonPrecedent,contrainte);
		//bouton suivant col 7 ligne 2-3
		chBoutonSuivant = new JButton(BoutonSuivant);
		chBoutonSuivant.addActionListener(this);
		contrainte.gridx = 4 ;
		contrainte.gridy = 1 ;
		this.add(chBoutonSuivant,contrainte);
		//titrefrise col 3-4 ligne 0
		contrainte.gridx = 2 ;
		contrainte.gridy = 0 ;
		this.add(titrefrise,contrainte);
		//contenu frise col 0 a 7 ligne 5-6-7
	
		JTable table = new JTable();
		table.setModel(new ModeleTable(NomDeLaFrise));
		table.getTableHeader().setBackground(new java.awt.Color(200,202,50));
		table.setRowHeight(50);
		table.getTableHeader().setResizingAllowed(false);
		table.getTableHeader().setReorderingAllowed(false);
		table.setAutoResizeMode(table.AUTO_RESIZE_OFF);
		JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(new Dimension(750,240));
		contrainte.gridx = 2 ;
		contrainte.gridy = 3 ;
		//contrainte.gridheight = 2 ;
		//contrainte.gridwidth = 3 ;
		this.add(scrollPane,contrainte);
		//traitement image	
		Panelimage.setLayout(CardLayoutImage) ;
		PanelText.setLayout(CardLayoutText);
		
		for (int b=0 ; b < intitules_images.length ; b++){
			tabLabels[b]= new JLabel(new ImageIcon("images"+File.separator+intitules_images[b]));
			tabLabelsTitre [b] = new JLabel(titreImage[b]) ;
			Panelimage.add(tabLabels[b]);
			PanelText.add(tabLabelsTitre[b]);
			contrainte.gridx = 2 ;
			contrainte.gridy = 1 ;
			this.add(Panelimage,contrainte);
			contrainte.gridx = 3 ;
			contrainte.gridy = 1 ;
			this.add(PanelText, contrainte);
		}
	}	
		public void actionPerformed (ActionEvent parEvt) {
			if(parEvt.getSource() == chBoutonPrecedent){
				this.CardLayoutImage.previous(Panelimage);
				this.CardLayoutText.previous(PanelText);
			}
			else if(parEvt.getSource() == chBoutonSuivant){
				this.CardLayoutImage.next(Panelimage);
				this.CardLayoutText.next(PanelText);			}
			else{
				
			}
	  }
}

